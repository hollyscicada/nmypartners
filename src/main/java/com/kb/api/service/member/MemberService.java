package com.kb.api.service.member;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.kb.api.persistence.MasterDao;
import com.kb.api.service.main.MainService;
import com.kb.api.util.Util;
@Service
public class MemberService {
	@Inject MasterDao MasterDao;
	@Inject MainService mainService;
	
	
	public String mychangeInfo(Model model, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException{ 
		
		String str = chkSession(session, request);
		if(!Util.chkNull(str)) {
			return str;
		}
		mainService.menu(model);
		
		return null;
	}
	public String mychangePw(Model model, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException{ 
		String str = chkSession(session, request);
		if(!Util.chkNull(str)) {
			return str;
		}
		mainService.menu(model);
		
		return null;
	}
	public String myPush(Model model, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException{ 
		String str = chkSession(session, request);
		if(!Util.chkNull(str)) {
			return str;
		}
		mainService.menu(model);
		
		return null;
	}
	public String myPurchase(Model model, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException{ 
		String str = chkSession(session, request);
		if(!Util.chkNull(str)) {
			return str;
		}
		mainService.menu(model);
		
		return null;
	}
	public String myCs(Model model, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException{ 
		String str = chkSession(session, request);
		if(!Util.chkNull(str)) {
			return str;
		}
		mainService.menu(model);
		
		return null;
	}
	public String myCsCreate(Model model, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException{ 
		String str = chkSession(session, request);
		if(!Util.chkNull(str)) {
			return str;
		}
		mainService.menu(model);
		
		/**
		 * 공통코드 가져오기
		 */
		HashMap paramMap = new HashMap();
		paramMap.put("up_code", "CS_CODE");
		List<HashMap<String, Object>> commonList = (List) MasterDao.dataList("mapper.CommonMapper", "list", paramMap);
		model.addAttribute("commonList", commonList);
		
		return null;
	}
	public String myFaq(Model model, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException{ 
		mainService.menu(model);
		return null;
	}
	public String myWith(Model model, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException{ 
		String str = chkSession(session, request);
		if(!Util.chkNull(str)) {
			return str;
		}
		mainService.menu(model);
		
		return null;
	}
	public String premium(Model model, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException{ 
		String str = chkSession(session, request);
		if(!Util.chkNull(str)) {
			return str;
		}
		mainService.menu(model);
		
		return null;
	}
	public String mypage(Model model, HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException{ 
		String str = chkSession(session, request);
		if(!Util.chkNull(str)) {
			return str;
		}
		mainService.menu(model);
		
		
		/**
		 * 내정보 member_seq 저장
		 */
		HashMap<String, Object> sessionMap = null;
		sessionMap = (HashMap<String, Object>) session.getAttribute("MEMBER");
		
		HashMap<String, Object> rstMap = new HashMap<String, Object>();
		rstMap.put("member_seq", sessionMap.get("member_seq"));
		
		 
		/**
		 * 폴더리스트
		 */
		List<HashMap<String, Object>> folderList = (List) MasterDao.dataList("mapper.FavoritesMapper", "folder_list", rstMap);
		
		for(int i = 0 ; i < folderList.size() ; i++) {
			folderList.get(i).put("favorites_list", (List) MasterDao.dataList("mapper.FavoritesMapper", "content_list", folderList.get(i)));
		}
		model.addAttribute("folderList", folderList);
		
		
		/**
		 * 최근본 컨텐츠 리스트
		 */
		List<HashMap<String, Object>> viewList = (List) MasterDao.dataList("mapper.ContentMapper", "view_content", rstMap);
		model.addAttribute("viewList", viewList);
		 
		/**
		 * 좋아요 리스트
		 */
		/*
		List<HashMap<String, Object>> goodsList = (List) MasterDao.dataList("mapper.ContentMapper", "goods_content", rstMap);
		model.addAttribute("goodsList", goodsList);
		*/
		/**
		 * 공유한 루틴
		 */
		List<HashMap<String, Object>> routineList = (List) MasterDao.dataList("mapper.ContentMapper", "routine_content", rstMap);
		model.addAttribute("routineList", routineList);
		
		/**
		 * 공유한 루틴
		 */
		List<HashMap<String, Object>> routineList2 = (List) MasterDao.dataList("mapper.ContentMapper", "routine_content2", rstMap);
		model.addAttribute("routineList2", routineList2);
		
		
		
		return null;
	}
	 
	/**
	 * 세션에 값 잇는지 체크후에 없다면 로그인페이지로 리다이렉트
	 * @param session
	 * @param request
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public String chkSession(HttpSession session, HttpServletRequest request) throws UnsupportedEncodingException {
		HashMap<String, Object> sessionMap = null;
		sessionMap = (HashMap<String, Object>) session.getAttribute("MEMBER");
		if(sessionMap == null) {
			String redirect_url = URLEncoder.encode(request.getRequestURI(), "UTF-8");
			session.setAttribute("redirect_url", redirect_url);
			return "redirect:/login?redirect_url="+redirect_url;
		}
		return null;
	}
}
