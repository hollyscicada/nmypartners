package com.kb.api.persistence;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;


@Repository
public class MasterDao {
	
	
	@Autowired
	@Qualifier("sqlSessionMaster")
	SqlSession sqlSessionMaster;
	
	
	
	@Autowired 
	@Qualifier("sqlSessionSlave")
	SqlSession sqlSessionSlave;
	
	
	public List<Object> dataList(String mapperNm, String queryNm, Object vo){
		return sqlSessionSlave.selectList(mapperNm+"."+queryNm,vo);
	}
	public Object dataRead(String mapperNm, String queryNm, Object vo){
		return sqlSessionSlave.selectOne(mapperNm+"."+queryNm,vo);
	}
	public int dataCreate(String mapperNm, String queryNm, Object vo){
		return sqlSessionMaster.insert(mapperNm+"."+queryNm,vo);
	}
	public int dataDelete(String mapperNm, String queryNm, Object vo){
		return sqlSessionMaster.update(mapperNm+"."+queryNm,vo);
	}
	public int dataUpdate(String mapperNm, String queryNm, Object vo){
		return sqlSessionMaster.update(mapperNm+"."+queryNm,vo);
	}
	public int dataCount(String mapperNm, String queryNm, Object map) {
		return sqlSessionMaster.selectOne(mapperNm+"."+queryNm,map);
	}
}
