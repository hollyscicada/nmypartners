package com.kb.api.controller.member;
 
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.model.SmsModel;
import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;
import com.kb.api.util.SmsSendUtil;
import com.kb.api.util.Util;

@RestController 
public class MemberRestController {
	
	@Inject MasterService masterService;
	@Inject SmsSendUtil smsSendUtil;
	   
	/**
	 * 휴대폰으로 인증번호 보내기 
	 */
	@PostMapping("/api/v1/member/auth/rand/number")  
	public ResponseEntity<Map<String, Object>> login(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		
		String rand = Util.makeAuthRandNumber();
		
		Map<String, String> dataMap = new HashMap<String, String>();
		dataMap.put("TARGET_ADDRESS", paramMap.get("phone").toString());
		dataMap.put("TITLE", "[라이프에이드] 인증번호가 발송되었습니다.");
		dataMap.put("CONTENTS",
				"안녕하세요, 라이프에이드 입니다.\n"
						+ "인증번호 : " + rand +"\n"
						+ "감사합니다."
		);
		SmsModel smsModel = null;
		smsModel = smsSendUtil.smsSend(dataMap);
		
		
		
		HashMap<String, Object> retMap = new HashMap<String, Object>();
		retMap.put("authNumber", rand);
		map.put("data", retMap);
		
		
		if(smsModel != null && smsModel.getResult_code() != null && smsModel.getResult_code().equals("1")) {
			map.put("success", true);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
}
