package com.kb.api.controller.board;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;

@RestController 
public class BoardRestController {   
	  
	@Inject MasterService masterService; 
	 
	   
	/**
	 * board 리스트 
	 */
	@PostMapping("/api/v1/board")   
	public ResponseEntity<Map<String, Object>> cs(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		
		String authToken = request.getHeader(Common.AUTHORIZATIONS);
		if(!StringUtils.isEmpty(authToken)) {
			String member_seq = new Common().getTokenInfo(request, "member_seq");
			param.put("member_seq", member_seq);
		}
		 
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		 
		param.setPage(param.get("page") != null ? Integer.parseInt(param.get("page").toString()) : param.getPage());
		param.setPage_block(param.get("page_block") != null ? Integer.parseInt(param.get("page_block").toString()) : param.getPage_block());
		param.put("pg", (Integer.parseInt(param.get("page").toString())-1)*param.getPage_block());  
		param.put("page_block", param.getPage_block()); 

		HashMap paramMap = new HashMap(param); 
		
		
		int record = masterService.dataCount("mapper.BoardMapper", "list_cnt", paramMap);
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.BoardMapper", "list", paramMap);
		param.setTotalCount(record); // 게시물 총 개수
		
		
		map.put("paging", param.getPageIngObj());
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * board 리스트 
	 */
	@PostMapping("/api/v1/board/{board_seq}")   
	public ResponseEntity<Map<String, Object>> cs(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param, @PathVariable String board_seq) throws Exception {
		
		String authToken = request.getHeader(Common.AUTHORIZATIONS);
		if(!StringUtils.isEmpty(authToken)) {
			String member_seq = new Common().getTokenInfo(request, "member_seq");
			param.put("member_seq", member_seq);
		}
		param.put("board_seq", board_seq);
		 
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		 
		HashMap paramMap = new HashMap(param); 
		HashMap<String, Object> retMap = (HashMap<String, Object>) masterService.dataRead("mapper.BoardMapper", "read", paramMap);
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/** 
	 * 게시글 삭제
	 */ 
	@PostMapping("/api/v1/board/del/{board_seq}")     
	public ResponseEntity<Map<String, Object>> notice_view(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param, @PathVariable String board_seq) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		param.put("board_seq", board_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		 
		HashMap paramMap = new HashMap(param);
		
		 
		int record = masterService.dataDelete("mapper.BoardMapper", "board_del", paramMap);
		
		if(record > 0) {
			map.put("success", true);
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}   
	   
	/**  
	 * 게시글 등록 
	 */ 
	@PostMapping("/api/v1/board/add")     
	public ResponseEntity<Map<String, Object>> board_add(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		 
		HashMap paramMap = new HashMap(param);
		
		 
		int record = masterService.dataDelete("mapper.BoardMapper", "board_add", paramMap);
		
		if(record > 0) {
			map.put("success", true);
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	
	/** 
	 * 게시글 수정
	 */ 
	@PostMapping("/api/v1/board/update/{board_seq}")     
	public ResponseEntity<Map<String, Object>> notice_update(HttpServletRequest request, HttpSession session, @RequestBody ProHashMap param, @PathVariable String board_seq) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		param.put("board_seq", board_seq);
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		 
		HashMap paramMap = new HashMap(param);
		
		 
		int record = masterService.dataDelete("mapper.BoardMapper", "board_update", paramMap);
		
		if(record > 0) {
			map.put("success", true);
		}
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}   
	
}
